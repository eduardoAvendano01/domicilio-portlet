import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { TabsPage } from '../pages/tabs/tabs';

import { MapaPage } from '../pages/mapa/mapa';
import { CapturaPage } from '../pages/captura/captura';

import { DomicilioPage } from '../pages/domicilio/domicilio';

import { DomicilioService } from '../services/domicilioService';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { Geolocation } from '@ionic-native/geolocation';
import { HttpModule } from '@angular/http';


@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    MapaPage, 
    CapturaPage,
    DomicilioPage
  ],
  imports: [
    HttpModule,
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    MapaPage, 
    CapturaPage,
    DomicilioPage
  ],
  providers: [
    Geolocation,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    {provide:DomicilioService,useClass:DomicilioService}
  ]
})
export class AppModule {}

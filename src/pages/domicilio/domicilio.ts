import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

declare var google;
/*
  Generated class for the Emision page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-domicilio',
  templateUrl: 'domicilio.html'
})
export class DomicilioPage {


    disableButton: string = "true";

    //@ViewChild('map') mapElement: ElementRef;
    map: any;

    //Variables model
    public selectedSegment: any;
    public domicilioObj: any;

    //Variables para librerias de google places y google maps
    marker: any;
    autocompleteInputValue: string;
    componentForm: any;
    tipoAsentamiento: any;
    

    //URL's servicios catalogos  sepomex
    baseURLFindByCP: string = 'http://107.23.246.132:8080/o/allianz-ws/catalogo.sepomex/findSepomexCodigoCP';
    baseURLFindEstados: string = 'http://107.23.246.132:8080/o/allianz-ws/catalogos.estados/findEstados';
    baseURLFindCiudadMunicipio: string = 'http://107.23.246.132:8080/o/allianz-ws/catalogos.sepomexestado/findSepomexEstadoMunicipioCiudad';
    baseURLFindAsentamiento: string = 'http://107.23.246.132:8080/o/allianz-ws/catalogo.sepomex/findSepomexByCodigoEstadoMunicipioTipoAsentamiento';
    baseURLFindCiudad: string = 'http://107.23.246.132:8080/o/allianz-ws/catalogos.sepomexestado/findSepomexEstadoCiudad';
 
    //Variables respuestas catalogos sepomex
    estados: any;
    municipiosCiudades: any;
    asentamientos: any;
    ciudades: any;

    //Variables model select
    codigoEstado: any;
    codigoMunicipio: any;
    codigoAsentamiento: any;
    codigoCiudad: any;

    constructor(public navCtrl: NavController, public geolocation: Geolocation, public http: Http, public alertCtrl: AlertController) { 

          this.selectedSegment = 'mapaSegment';

          this.componentForm = {
              postal_code: 'short_name', //codigo postal
              route: 'long_name', //nombre calle
              street_number: 'short_name', //numero calle
              administrative_area_level_1: 'long_name', //Estado
              locality: 'long_name', //Ciudad
              sublocality_level_1: 'short_name', //Colonia
              administrative_area_level_3: 'short_name' //Delegacion
          };

          this.domicilioObj = {
              postal_code: '', //codigo postal
              route: '', //nombre calle
              street_number: '', //numero calle
              administrative_area_level_1: '', //Estado
              locality: '', //Ciudad
              sublocality_level_1: '', //Colonia
              administrative_area_level_3: '', //Delegacion
              num_int: '', //Numero interior
              latitude: '',
              longitude: ''
          }

          //FUNCION PARA OBTENER DATOS DE WINDOW PORTLET
          window['LibMapaAllianz'] = {};
          window['LibMapaAllianz'].obtenerDireccionMapa = function () {
            console.log('obtenerDireccionMapa desde javascript');
            return this.valoresDireccion();
          }

          //CARGA INICIAL DE CATALOGOS

          /*

          //Obtiene estados
          this.http.get(this.baseURLFindEstados).map(res => res.json()).subscribe(data => {

            this.estados = data;
            this.codigoEstado = data[0].codigoEstado;

            //Obtiene municipios y ciudades por estado
            this.http.get(this.baseURLFindCiudadMunicipio + "?codigoEstado=" + this.codigoEstado).map(res => res.json()).subscribe(data => {
              this.municipiosCiudades = data;
              this.codigoMunicipio = data[0].codigoMunicipio;

                //Obtiene asentamientos
                //Seteando valor de tipo de asentamiento
                this.tipoAsentamiento = 9;
                this.http.get(this.baseURLFindAsentamiento + "?codigo_estado=" + this.codigoEstado + "&codigo_municipio=" + this.codigoMunicipio + "&codigo_tipo_asentamiento=" + this.tipoAsentamiento).map(res => res.json()).subscribe(data => {
                    this.asentamientos = data;
                    this.codigoAsentamiento = data[0].idAsentamientoCP;
                });

                //Obtiene ciudades
                this.http.get(this.baseURLFindCiudad + "?codigoEstado=" + this.codigoEstado).map(res => res.json()).subscribe(data => {
                    this.ciudades = data;
                    this.codigoCiudad = data[0].ciudad;
                });
            });

          });*/


    }
 
    ionViewDidLoad(){
      console.log("On ion view");
      this.loadMap();
      this.autocomplete();
    }

    loadMap(){

      //Se crea mapa con coordenadas del centro de CDMX
      let latLng = new google.maps.LatLng(19.436161, -99.137314);
  
      let mapOptions = {
        center: latLng,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true
      }

      let mapa = document.getElementById("map");
      this.map = new google.maps.Map(mapa, mapOptions);

      //Se actualiza centro del mapa con geolocalizacion
      this.geolocation.getCurrentPosition().then((position) => {

        let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

        let mapOptions = {
          center: latLng,
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          disableDefaultUI: true
        }

        this.map = new google.maps.Map(mapa, mapOptions);

      }, (err) => {
        console.log(err);
      });
  
    }

    loadMap2() {

      let mapa = document.getElementById("map");

      //Carga mapa con coordenadas default centro de cdmx
      let latLng = new google.maps.LatLng(this.domicilioObj.latitudMapa, this.domicilioObj.longitudMapa);

      let mapOptions = {
        center: latLng,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true
      }

      this.map = new google.maps.Map(mapa, mapOptions);

    }

    autocomplete() {

      //Se inicializa input con predicciones de google places
      let input_autocomplete = document.getElementById("autocomplete");
      let autocomplete = new google.maps.places.Autocomplete(input_autocomplete, { componentRestrictions: { country: "mx" } });

      //Se crea listener para cambios de places
      google.maps.event.addListener(autocomplete, 'place_changed', () => {
          console.log("On place changed");

          if (autocomplete.getPlace().geometry !== undefined) {

              let place = autocomplete.getPlace();
            
              console.log(place);

              console.log("Latitud " + place.geometry.location.lat());
              console.log("Longitud " + place.geometry.location.lng());


              this.domicilioObj.latitude = place.geometry.location.lat();
              this.domicilioObj.longitude = place.geometry.location.lng();

              //Obtenemos lat long del punto de places
              let latLng = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());

              //Reasignando centro del mapa con el punto de places
              let mapOptions = {
                center: latLng,
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                disableDefaultUI: true
              }

              let mapa = document.getElementById("map");
              this.map = new google.maps.Map(mapa, mapOptions);

              //Creando marker
              var image = 'assets/img/allianz-pin.png';
              this.marker = new google.maps.Marker({
                map: this.map,
                animation: google.maps.Animation.DROP,
                position: this.map.getCenter(),
                icon: image
              });


               //Se agrega listener para click del marker
              google.maps.event.addListener(this.marker, 'click', () => {

                

                
                let alert = this.alertCtrl.create();
                alert.setTitle('Número Interior');
                alert.setSubTitle('Si tiene un número interior ingréselo, de otro modo sólo presione siguiente.');

                alert.addInput({
                  type: 'input',
                  label: 'Numero Interior:',
                  value: '',
                });


                alert.addButton({
                  text: 'Siguiente',
                  handler: data => {
                    console.log(' data:');
                    console.log(data[0]);

                    this.domicilioObj.num_int = data[0];

                    this.selectedSegment = 'capturaSegment';

                    this.onSegmentChanged();
                  }
                });


                alert.present();
              });


              //Limpiando valores de domicilio object
              this.domicilioObj.postal_code = ''; 
              this.domicilioObj.route = ''; 
              this.domicilioObj.street_number = '';
              this.domicilioObj.administrative_area_level_1 = ''; 
              this.domicilioObj.locality = ''; 
              this.domicilioObj.sublocality_level_1 = ''; 
              this.domicilioObj.administrative_area_level_3 = ''; 
              this.domicilioObj.num_int = '';

              //Iterando objeto place para obtener direccion estructurada
              for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (this.componentForm[addressType]) {
                  var addressDetail = place.address_components[i][this.componentForm[addressType]];
                  console.log("Address detail "+addressDetail+" address type "+addressType);

                  this.domicilioObj[addressType] = addressDetail;
                }
              }

              console.log("Domicilio Obj");
              console.log(this.domicilioObj);
          }    
      });
    }


    limpiarAutocomplete() {
      this.autocompleteInputValue = null;

      //Si el campo es vacio elimino marker del mapa
      if (this.autocompleteInputValue === "" || this.autocompleteInputValue === null) {
        console.log("if vacio");

        if( this.marker !== undefined && this.marker !== null){
          this.marker.setMap(null);
        }
      }
    }

    blurAutocompleteInput(inputValue) {

      //Si el campo es vacio elimino marker del mapa
      if (inputValue === "" || inputValue === null) {
        console.log("if vacio");

        if( this.marker !== undefined && this.marker !== null){
          this.marker.setMap(null);
        }
      }

    }

    clickCapturaSegment(){
      console.log("Click capturasegment");
    }

    
  onSegmentChanged(){
      console.log("Change "+this.selectedSegment);

      if(this.selectedSegment === 'capturaSegment' && this.autocompleteInputValue!== '' ){
          console.log("En captura if");


          //CARGA DE CATALOGOS SEPOMEX

          if ( this.domicilioObj.postal_code !== '' && this.domicilioObj.postal_code !== null) {
            console.log("En if codigo postal");
            console.log("Codigo postal " + this.domicilioObj.postal_code);

            //Obtiene datos por cp
            this.http.get(this.baseURLFindByCP + "?codigo_cp=" + this.domicilioObj.postal_code).map(res => res.json()).subscribe(data => {
              console.log("***Data");
              console.log(data);
              this.codigoEstado = data[0].codigoEstado;
              console.log(this.codigoEstado);
              this.codigoMunicipio = data[0].codigoMunicipio;
              this.codigoCiudad = data[0].ciudad;
              this.tipoAsentamiento = data[0].codigoTipoAsentamiento;


              //Obtiene municipios y ciudades por estado
              this.http.get(this.baseURLFindCiudadMunicipio + "?codigoEstado=" + this.codigoEstado).map(res => res.json()).subscribe(data => {

                this.municipiosCiudades = data;

              });

              //Obtiene asentamientos
              this.http.get(this.baseURLFindAsentamiento + "?codigo_estado=" + this.codigoEstado + "&codigo_municipio=" + this.codigoMunicipio + "&codigo_tipo_asentamiento=" + this.tipoAsentamiento + "&codigo_CP="+this.domicilioObj.postal_code).map(res => res.json()).subscribe(data => {

                this.asentamientos = data;

                this.codigoAsentamiento = data[0].idAsentamientoCP;
                for (var i = 0; i < data.length; i++) {
                  if (data[i].asentamiento === this.domicilioObj.sublocality_level_1) {
                    this.codigoAsentamiento = data[i].idAsentamientoCP;
                    break;
                  }

                }

              });

              //Obtiene ciudades
              this.http.get(this.baseURLFindCiudad + "?codigoEstado=" + this.codigoEstado).map(res => res.json()).subscribe(data => {

                this.ciudades = data;

              });


            });

            //Obtiene estados
            this.http.get(this.baseURLFindEstados).map(res => res.json()).subscribe(data => {
              console.log(data[0].descripcion);

              this.estados = data;
            });



          } else {

            //Limpia valores ingresados previamente
            this.codigoEstado = "";
            this.codigoMunicipio = "";
            this.codigoCiudad = "";
            this.codigoAsentamiento = "";
            this.tipoAsentamiento = "";

            this.http.get(this.baseURLFindEstados).map(res => res.json()).subscribe(data => {

              this.estados = data;
              this.codigoEstado = data[0].codigoEstado;

              //Obtiene municipios y ciudades por estado
              this.http.get(this.baseURLFindCiudadMunicipio + "?codigoEstado=" + this.codigoEstado).map(res => res.json()).subscribe(data => {
                this.municipiosCiudades = data;
                this.codigoMunicipio = data[0].codigoMunicipio;

                //Obtiene asentamientos
                //Seteando valor de tipo de asentamiento
                this.tipoAsentamiento = 9;
                this.http.get(this.baseURLFindAsentamiento + "?codigo_estado=" + this.codigoEstado + "&codigo_municipio=" + this.codigoMunicipio + "&codigo_tipo_asentamiento=" + this.tipoAsentamiento).map(res => res.json()).subscribe(data => {
                  this.asentamientos = data;
                  this.codigoAsentamiento = data[0].idAsentamientoCP;
                });

                //Obtiene ciudades
                this.http.get(this.baseURLFindCiudad + "?codigoEstado=" + this.codigoEstado).map(res => res.json()).subscribe(data => {

                  this.ciudades = data;
                  this.codigoCiudad = data[0].ciudad;

                });

              });

            });

          }

          this.validarForma();
      }else if( this.selectedSegment === 'mapaSegment' ){
          console.log("En mapa event capturado");

          if (this.autocompleteInputValue !== null && this.autocompleteInputValue !== '' && this.autocompleteInputValue !== undefined) {

            console.log("En if");

            console.log("**autocompleteInputValue " + this.autocompleteInputValue);

            this.loadMap2();
            this.autocomplete();

            let mapa = document.getElementById("map");

            //Obtenemos lat long del punto de places
            let latLng = new google.maps.LatLng(this.domicilioObj.latitude, this.domicilioObj.longitude);


            //Reasignando centro del mapa con el punto de places
            let mapOptions = {
              center: latLng,
              zoom: 15,
              mapTypeId: google.maps.MapTypeId.ROADMAP,
              disableDefaultUI: true
            }


            this.map = new google.maps.Map(mapa, mapOptions);

            //Creando marker
            var image = 'assets/img/allianz-pin.png';
            this.marker = new google.maps.Marker({
              map: this.map,
              animation: google.maps.Animation.DROP,
              position: this.map.getCenter(),
              icon: image
            });

            //Se agrega listener para click del marker
            google.maps.event.addListener(this.marker, 'click', () => {


              let alert = this.alertCtrl.create();
              alert.setTitle('Número Interior');
              alert.setSubTitle('Si tiene un número interior ingréselo, de otro modo sólo presione siguiente.');

              alert.addInput({
                type: 'input',
                label: 'Numero Interior:',
                value: '',
              });


              alert.addButton({
                text: 'Siguiente',
                handler: data => {
                  console.log(' data:');
                  console.log(data);

                  this.domicilioObj.num_int = data[0];

                  this.selectedSegment = 'capturaSegment';

                  this.onSegmentChanged();
                  
                }
              });


              alert.present();
            });

          }
      }
  }


  /*FUNCIONES*/


  blurCodigoPostal() {
    this.validarForma();
  }

  blurNombreCalle() {
    this.validarForma();
  }

  blurNumeroCalle() {
    this.validarForma();
  }

  blurNumeroInterior() {
    this.validarForma();
  }

  changeColonia() {
    console.log("En change colonia");
    this.validarForma();
  }

  changeDelegacionMunicipio() {
    console.log("En change municipio");

    //Obtiene asentamientos
    //Seteando valor de tipo de asentamiento
    console.log("Codigo estado " + this.codigoEstado + " cod municipio " + this.codigoMunicipio);
    this.tipoAsentamiento = 9; //Tipo: colonia
    this.http.get(this.baseURLFindAsentamiento + "?codigo_estado=" + this.codigoEstado + "&codigo_municipio=" + this.codigoMunicipio + "&codigo_tipo_asentamiento=" + this.tipoAsentamiento).map(res => res.json()).subscribe(data => {
      this.asentamientos = data;
      this.codigoAsentamiento = data[0].idAsentamientoCP;
    });

    this.validarForma();

  }

  changeCiudad() {
    console.log("En change ciudad");

    this.validarForma();
  }

  changeEstado() {
    console.log("En change estado " + this.codigoEstado);

    //Obtiene municipios y ciudades por estado
    this.http.get(this.baseURLFindCiudadMunicipio + "?codigoEstado=" + this.codigoEstado).map(res => res.json()).subscribe(data => {
      this.municipiosCiudades = data;
      this.codigoMunicipio = data[0].codigoMunicipio;

      //Obtiene asentamientos
      //Seteando valor de tipo de asentamiento
      this.tipoAsentamiento = 9;
      this.http.get(this.baseURLFindAsentamiento + "?codigo_estado=" + this.codigoEstado + "&codigo_municipio=" + this.codigoMunicipio + "&codigo_tipo_asentamiento=" + this.tipoAsentamiento).map(res => res.json()).subscribe(data => {
        this.asentamientos = data;
        this.codigoAsentamiento = data[0].idAsentamientoCP;
      });


    });


    //Obtiene ciudades
    this.http.get(this.baseURLFindCiudad + "?codigoEstado=" + this.codigoEstado).map(res => res.json()).subscribe(data => {

      this.ciudades = data;
      this.codigoCiudad = data[0].ciudad;

    });

    this.validarForma();
  }

  //Directiva codigo postal
  onKeyPress(e, codigoPostal) {
    //console.log("entra a on keyup "+this.codigoPostal);
    if (this.domicilioObj.postal_code .length > 0) {
      //console.log("model--"+this.codigoPostal );
      var res = this.domicilioObj.postal_code .charAt(this.domicilioObj.postal_code .length - 1);
      //console.log("Antes de if "+res.match('[0-9.]'));
      if (!res.match('[0-9.]')) {
        //console.log("antes de slice--"+this.codigoPostal );
        this.domicilioObj.postal_code  = this.domicilioObj.postal_code .slice(0, -1);
        //console.log("despues de slice--"+this.codigoPostal );
      }
    }
    //console.log("antes");
    this.domicilioObj.postal_code  = this.domicilioObj.postal_code.replace(/\D/g, '');
    //console.log("despues");
  }


  validarForma() {

    console.log("En validar forma " + this.domicilioObj.postal_code);

    if (this.domicilioObj.postal_code !== '' && this.domicilioObj.postal_code !== null && this.domicilioObj.route !== '' && this.domicilioObj.route !== null && this.domicilioObj.street_number !== '' && this.domicilioObj.street_number !== null) {
      console.log("En if");
      this.disableButton = "false";
    } else {
      this.disableButton = "true";
    }

  }


  //Push a siguiente página del flujo
  siguientePagina() {
    //this.navCtrl.push(PerfilTransPage);
    
  }

  /*
  selectedMap(){
      console.log("selectedMap");
      //this.loadMap();
  }*/

}

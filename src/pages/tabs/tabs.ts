import { Component } from '@angular/core';

import { MapaPage } from '../mapa/mapa';
import { CapturaPage } from '../captura/captura';

import { DomicilioService } from '../../services/domicilioService';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

    tabMapaRoot = MapaPage;
    tabCapturaRoot = CapturaPage;

    constructor(private domicilioForm: DomicilioService) {

    }

    capturaPageSelected() {
    this.domicilioForm.fireTabSelectedEvent();
  }

    mapaPageSelected() {
      console.log("En mapa page selected");
      this.domicilioForm.fireMapaTabSelectedEvent();
    }
}

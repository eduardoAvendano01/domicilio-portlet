/**
 * Copyright 2000-present Liferay, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ionic.domicilio;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

/**
 * @author Liferay
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.css-class-wrapper=portlet-jsp",
		"com.liferay.portlet.display-category=Ionic",
		"com.liferay.portlet.header-portlet-css=/ionic/build/main.css",
		"com.liferay.portlet.header-portlet-javascript=https://maps.google.com/maps/api/js?key=AIzaSyC5DkEDsylPfwija_Ps1GWfFm9cO87BbXo&libraries=places",
		"com.liferay.portlet.footer-portlet-javascript=/ionic/build/polyfills.js",
		"com.liferay.portlet.footer-portlet-javascript=/ionic/build/vendor.js",
		"com.liferay.portlet.footer-portlet-javascript=/ionic/build/main.js",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=Domicilio",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/ionic/index.jsp",
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class MainPortlet extends MVCPortlet {
}